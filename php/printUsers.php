<?php 

    require 'dbconnection.php';

    $sql = 'SELECT * FROM users ORDER BY city';
    $query = $conn->query($sql);
    $users = [];
    while ($row=$query->fetch()) {

        $content = [
            'id'=>$row['id'],
            'name'=>$row['name'],
            'surname'=>$row['surname'],
            'city'=>$row['city']
        ];
        array_push($users,$content);
       
    }
    echo json_encode($users);
    
?>