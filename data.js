let inputBtn = document.querySelector('#inputUser');
let printBtn = document.querySelector('#printUser');
let printDiv = document.querySelector('#printDiv');
let errMsg = document.querySelector('#errorMsg');
let successMsg = document.querySelector('#successMsg');
inputBtn.addEventListener('click',function(e){
    if (errMsg) {
        errMsg.innetText = '';  
    }
    if (successMsg) {
        successMsg.innerText = '';
    }
    printDiv.innerHTML = `
    <form id="saveUser" action="./php/saveUser.php" method="POST">
        <div class="form-group">
            <label for="name">Име</label>
            <input type="text" class="form-control" id="name" name="name">
            <p id="nameErr"></p>
        </div>
        <div class="form-group">
            <label for="surname">Презиме</label>
            <input type="text" class="form-control" id="surname" name="surname">
            <p id="surnameErr"></p>
        </div>
        <div class="form-group">
            <label for="city">Град</label>
            <input type="text" class="form-control" id="city" name="city">
            <p id="cityErr"></p>
        </div>
        
        <button type="submit" class="btn btn-primary">Зачувај</button>
  </form>
    
    `
})
printBtn.addEventListener('click',function(e){
    if (errMsg) {
        errMsg.innetText = '';  
    }
    if (successMsg) {
        successMsg.innerText = '';
    }
    fetch('php/printUsers.php')
    .then(function(response){
        return response.json();
    })
    .then(function(data){
       
        
        printDiv.innerHTML = `
        <table class="table">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Име</th>
                    <th scope="col">Презиме</th>
                    <th scope="col">Град</th>
                </tr>
            </thead>
            <tbody id="tableBody">
               
            </tbody>
        </table>
            
        `
        let tableBody = document.querySelector('#tableBody');
        let content = '';
        data.forEach(user=>{
            content += `
            <tr>
                <th scope="row">${user.id}</th>
                <td>${user.name}</td>
                <td>${user.surname}</td>
                <td>${user.city}</td>
            </tr>
            `
        });
        tableBody.innerHTML = content;
       
    })
    .catch(function(err){
        console.log(err);
    })
   
   
})


