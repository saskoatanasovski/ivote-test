<?php

    try {
        $conn = new PDO("mysql:host=localhost;dbname=usersdb;","root","",
        [PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_ERRMODE=>PDO::ERRMODE_EXCEPTION]);
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
?>